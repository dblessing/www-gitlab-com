---
layout: markdown_page
title: "Field Marketing"
---

## Welcome to the Field Marketing Handbook!

## On this page
* [Events overview](#events)
* [Events goals](#goals)
* [Evaluating events](#evaluate)
* [Promoting events](#promotion)
* [Booth guidelines](#guidelines)
* [Networking tips](#networking)
* [Events attire](#attire)
* [Booth staffing](#boothstaffing)
* [Event followup](#followup)
* [Speaker information](#speakerportal)
* [Meetups](#meetups)
* [Swag](#swag)


## **Field Marketing**

Field marketing includes event marketing and swag production.

## Events Overview<a name="events"></a>

* Get GitLab team members out to meet people in the community. People that you meet in person are more likely to become evangelists for you.
* If you go talk at small events you'll see that over time you get into bigger ones.

## Event Goals<a name="goals"></a>
- Evangelism (brand) - Talk to as many people as we can about GitLab. Our booth presence we must be friendly, knowledgeable, and outgoing.
- Sales leads (revenue) - We will collect business cards and email addresses for potential leads.
- Hiring (company) - Always be recruiting. See someone doing a great job of evangelism for another product? Ask that person for coffee.
- Partnership (channel) - Organizations adding support for GitLab and/or shipping GitLab with their offering.
- Product Direction - discovery of what the market asking for, in need of and willing to pay for.
- Drive audience to any talks/ events we are having.

## How We Evaluate Events<a name="evaluate"></a>
- Price/ Budget
- Location- tops cities for developers and can we tag on another event?
- Attendees- who’s attending? Is it the right audience for us? entrepreneurs vs engineers.
- Size- try to reach a large audience.
- What will our presence be? Who can/ can we go, participate, speak, booth?
- Can we pair this event with other local engagements?
- We want to strike a good balance between: community and enterprise events.

## For GitLab Employees<a name="promotion"></a>
- If there is an event you would like to attend, are attending, speaking or have proposed a talk please let the Field Marketing Manager know. We will try to support you attending the event in any way we can. Even if we are unable to sponsor the event you can likely still attend with your managers approval.
- Remember we are way more likely to sponsor an event if you have solidified a speaking spot. So, if there is something you really want to attend propose a talk.
- Calls for papers are posted in the CFP Channel on Slack.
- If you do not have any GitLab swag and are going to an event, notify the Field Marketing Manager as soon as possible so we can try and get you a GitLab shirt to wear.

## Promoting Events<a name="promotion"></a>
- Use social media to post as soon as committed to attending an event.
- Email signatures- starting 1 month before event for those attending event.
- Monthly blog post about upcoming events at the first of the month.

## Pre Event Outreach<a name="promotion"></a>
- Get attendee list and contact customers and prospects before event.
- If there is not an attendee list do "warm" outreach to prospects- create email campaign based on location. Send one email 1.5 weeks before event.
- BDRS will help set up in person meetings to occur at event.

## Complete Before Event<a name="promotion"></a>
- Set up post event follow up campaigns in Marketo.
- After Event Survey created and email template ready to go out first work day back after event. Includes details on lead followup and how to add business card details.
- Social media to go out during event scheduled.
- Plan for what to do with any remaining swag.

## **At Events**

## Employee Booth Guidelines<a name="guidelines"></a>

- If you see someone standing alone, talk to them.
- Do not stand around and talk to other GitLab coworkers. Talk to people you don’t know.
- Do not sell; generate interest to learn more. Attendees have a lot of info they are digesting, so get their info and some key info to follow up on.
- Give out swag and one-pagers!
- Don’t be afraid to walk around to other booths and talk to people. Make friends we could partner with, create interesting content with, or just have friendly beers.
- The booth should be clean and organized at all times.
- Document any product feedback you get.
- If press comes to the event feel free to put them in contact with CEO (Sid) or Marketing (Ashley).
- Bring your business cards and a plan to take notes on encounters.

## Networking Tips<a name="networking"></a>
- Listen.
- Ask questions.
- Don't interrupt.
- Everyone gets shy sometimes, be the brave one and introduce yourself to new people.
- Feel free to talk about our next release, GitLab [strategy](https://about.gitlab.com/strategy/), or our [product direction](https://about.gitlab.com/direction/#vision).

## Booth Set Up<a name="setup"></a>

- To Bring:
    - Generic business cards
    - [Lego business card holder] (https://gitlab.com/gitlab-com/marketing/issues/306#note_12536262)
    - Stickers + any other swag
    - Events lap top (for slide show) + charger
    - Back up power banks
    - Gum

## Suggested Attire<a name="attire"></a>
- Wear at least one piece of branded GitLab clothing. If you prefer to wear something dressier than the GitLab branded items available that is also acceptable. 
- If the conference is business casual try some nice jeans (no holes) or pants.
- Clean, closed-toed shoes please.
- A smile.

## Booth Staffing<a name="boothstaffing"></a>
- Ideally booth shifts will be around 3 hours or less.
- Staff more people during peak traffic hours.
- Avopid shift changes during peak hours.
- Aim to staff booth with individuals with a variety of expertise- ideally multiple deparments will be represented.
- Send out calendar invites to booth staff with the following information:
    - Time and date of event, booth, and shift
    - Suggested attire
    - How he/ she can find his/ her ticket
    - Any instructions on using or locating lead scanner
    - Let them know of any contests happening
    - How to get a hold of you
    - Link to events handbook
    - Any relevant event set up or clean up

## After an Event<a name="followup"></a>

- Fill out after event survey.
- Add event debrief to event issue in marketing project. The debrief should include the following if applicable:
    - Was the event valuable?
        - Would you go again/ should we go again?
        - Did we get good leads/ contacts?
        - Best questions asked and conversations
        - Was our sponsorship/ involvement successful?
    - How was the booth set up?
        - How was the booth staffing?
        - Did the booth get enough traffic?
        - Booth location and size
    - How did our swag go over?
        - Did we have enough/ too much?
    - Contests
        - Did the contest(s) effectively build our brand and connecting with our target audience?
- Make sure contacts/ leads gathered from event are with marketing and categorized under specific event campaign.
- Sales team and BDR's will have 3 days after event lead list sent to claim leads and add notes before bulk after event outreach will occur.
- Follow up with leads from event within 5 business days after event ends.

## Speaker Portal<a name="speakerportal"></a>

* Catalogue of talks and speaker briefs we have done or can do and their run times.  Contact community@gitlab.com for access.

## Meetups<a name="meetups"></a>

- Ideally first meetups should be run by GitLab employees. If someone manages to have 3-4 successive events, the meetup itself will live on. It is much harder to start new meetups versus maintaining existing ones. So we should everything to keep existing events going.
- We love to attend others meetup groups. If you would like someone from the GitLab team to stop by your event please email community@gitlab.com.

## Swag<a name="swag"></a>

* We aim to have our swag delight and/ or be useful.
* We aim to make limited edition and themed swag for the community to collect. Bigger events will have custom tanuki stickers in small runs, only available at specific event.
* We aim to do swag in a way that doesn't take a lot of time to execute => self serve => [web shop](https://gitlab.mybrightsites.com/)
* With a web shop you can just give people credit, they can pick what they want and provide shipping info.
* Of course we love [stickers](http://opensource.com/business/15/11/open-source-stickers). We are working on special edition stickers for contributors to be announced May 2016- being shipped right now.
